<?php

namespace ATM\CompetitionBundle\Helpers;

class Canonicalizer
{
    public static function canonicalize($string, $preserve_caps = false)
    {
        $chars2replace = array(
            "Á" => "a",
            "À" => "a",
            "Ä" => "a",
            "Â" => "a",
            "á" => "a",
            "à" => "a",
            "ä" => "a",
            "â" => "a",
            "É" => "e",
            "È" => "e",
            "Ë" => "e",
            "Ê" => "e",
            "é" => "e",
            "è" => "e",
            "ë" => "e",
            "ê" => "e",
            "Í" => "i",
            "Ì" => "i",
            "Ï" => "i",
            "Î" => "i",
            "í" => "i",
            "ì" => "i",
            "ï" => "i",
            "î" => "i",
            "Ó" => "o",
            "Ò" => "o",
            "Ö" => "o",
            "Ô" => "o",
            "ó" => "o",
            "ò" => "o",
            "ö" => "o",
            "ô" => "o",
            "ō" => "o",
            "Ú" => "u",
            "Ù" => "u",
            "Ü" => "u",
            "Û" => "u",
            "ú" => "u",
            "ù" => "u",
            "ü" => "u",
            "û" => "u",
            "ū" => "u",
            "Ñ" => "n",
            "ñ" => "n",
            //"ß" => "B", -> this one turns the "ss" into "B"
            "º" => "o",
            "ª" => "a",
            "ç" => "c",
            "Ç" => "c",
            " " => ""
        );

        //$string = strtolower($string);
        if(!$preserve_caps)
        {
            $string = mb_strtolower($string,mb_detect_encoding($string));
        }
        $string = self::convert_smart_quotes($string);
        //$string = str_replace(array('...',':','"','.',',','&','?','!','´','\'','%','/'),'', $string);
        $string = str_replace(array('...',':','"'),'', $string);

        foreach($chars2replace as $i=>$u) {
            $string = mb_eregi_replace($i,$u,$string);
            //dump('Replacing '.$i.' by '.$u.' => '.$string);
        }
        
        //iconv("utf-8","ascii//TRANSLIT",$input);
        $string = preg_replace('/\s+/','',$string);
        $string = preg_replace('/\./','',$string);
        $string = preg_replace('/\,/','',$string);
        $string = preg_replace('/\&/','',$string);
        $string = preg_replace('/\?/','',$string);
        $string = preg_replace('/\!/','',$string);
        $string = preg_replace('/\´/','',$string);
        $string = preg_replace('/\`/','',$string);
        $string = preg_replace('/\'/','',$string);
        $string = preg_replace('/\%/','',$string);
        $string = preg_replace('/\//','',$string);
        //return strtolower($string);
        return $string;
    }
    
    public static function convert_smart_quotes($string)
    { 
        $search = array(chr(145), 
                        chr(146), 
                        chr(147), 
                        chr(148), 
                        chr(151)); 

        $replace = array("'", 
                         "'", 
                         '"', 
                         '"', 
                         '-'); 

        return str_replace($search, $replace, $string); 
    }

    public static function slugify($str, $separator)
    {
        // Código copiado de http://cubiq.org/the-perfect-php-clean-url-generator
        $slug = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
        $slug = strtolower(trim($slug, $separator));
        $slug = preg_replace("/[\/_|+ -]+/", $separator, $slug);
        return $slug;
    }
}