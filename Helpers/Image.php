<?php

namespace ATM\CompetitionBundle\Helpers;

use \Imagick;

class Image{
    public static function removeExif($source_file, $destination_file = false)
    {
        $destination_file = $destination_file ? $destination_file : $source_file;
        $exif = new Imagick($destination_file);
        $profiles = $exif->getImageProfiles("icc", true);
        $exif->stripImage();
        if(!empty($profiles))
        {
            $exif->profileImage("icc", $profiles['icc']);
        }
        $exif->writeImage($destination_file);
    }

    public static function correctImageOrientation($source_file, $destination_file = false)
    {
        $destination_file = $destination_file ? $destination_file : $source_file;
        if(function_exists('exif_read_data'))
        {
            $exif = exif_read_data($source_file);
            if($exif && isset($exif['Orientation']))
            {
                $orientation = $exif['Orientation'];
                if($orientation != 1)
                {
                    $img = imagecreatefromjpeg($source_file);
                    $deg = 0;
                    switch ($orientation) {
                        case 3:
                            $deg = 180;
                            break;
                        case 6:
                            $deg = 270;
                            break;
                        case 8:
                            $deg = 90;
                            break;
                    }
                    if ($deg)
                    {
                        $img = imagerotate($img, $deg, 0);
                    }
                    // then rewrite the rotated image back to the disk as $destination_file
                    imagejpeg($img, $destination_file, 95);
                } // if there is some rotation necessary
            } // if have the exif orientation info
        } // if function exists
    }

    public static function getFirstFrameImageFromGif($gif_file,$destination_folder){

        $imagick = new Imagick($gif_file);

        $imagick = $imagick->coalesceImages();

        foreach ($imagick as $frame) {
            $filename = md5(uniqid()).'.jpg';
            $frame->writeImage($destination_folder.$filename);
            return $filename;
        }

        return null;
    }
}