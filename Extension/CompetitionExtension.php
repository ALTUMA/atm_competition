<?php

namespace ATM\CompetitionBundle\Extension;

use Doctrine\ORM\EntityManagerInterface;
use ATM\CompetitionBundle\Services\SearchVotes;
use ATM\CompetitionBundle\Services\SearchCompetition;
use ATM\CompetitionBundle\Services\SearchContestants;
use \DateTime;

class CompetitionExtension extends \Twig_Extension{
    private $em;
    private $config;
    private $searchVotes;
    private $searchCompetition;
    private $searchContestants;

    public function __construct(EntityManagerInterface $em,SearchVotes $searchVotes, SearchCompetition $searchCompetition,SearchContestants $searchContestants,$atm_competition_config)
    {
        $this->em = $em;
        $this->config = $atm_competition_config;
        $this->searchVotes = $searchVotes;
        $this->searchCompetition = $searchCompetition;
        $this->searchContestants = $searchContestants;
    }

    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('ATMCompIsContestant', array($this, 'isContestant')),
            new \Twig_SimpleFunction('ATMCompGetLastUserMedia', array($this, 'getLastUserMedia')),
            new \Twig_SimpleFunction('ATMCompIsRegisterPeriod', array($this, 'isRegisterPeriod')),
            new \Twig_SimpleFunction('ATMCompIsCompetitionActive', array($this, 'isCompetitionActive')),
            new \Twig_SimpleFunction('ATMCompContestantTotalVotes', array($this, 'contestantTotalVotes')),
            new \Twig_SimpleFunction('ATMGetCurrentCompetition', array($this, 'getCurrentCompetition')),
            new \Twig_SimpleFunction('ATMGetCompetitionContestants', array($this, 'getCompetitionContestants')),
            new \Twig_SimpleFunction('ATMUserAlreadyVoted', array($this, 'userAlreadyVoted')),
            new \Twig_SimpleFunction('ATMCompGetBigMainImagePath', array($this, 'getBigImagePath')),
            new \Twig_SimpleFunction('ATMCompIsFinished', array($this, 'getCompetitionIsFinished')),
        );
    }


    public function isContestant($competitionId,$userId){
        $contestant = $this->em->getRepository('ATMCompetitionBundle:Contestant')->findOneBy(array(
            'competition' =>$competitionId,
            'user' => $userId
        ));

        return is_null($contestant) ? false : true;
    }

    public function getLastUserMedia($maxResults = null,$userId){
        $entityNamespace = $this->config['media_entity']['namespace'];
        $qbIds = $this->em->createQueryBuilder();

        $qbIds
            ->select('m.id')
            ->from($entityNamespace,'m')
            ->join('m.'.$this->config['media_entity']['user_field_name'],'u','WITH',$qbIds->expr()->eq('u.id',$userId))
            ->orderBy('m.'.$this->config['media_entity']['order_by_field'],$this->config['media_entity']['order_by_direction']);


        $query = $qbIds->getQuery();
        if($maxResults){
            $query->setMaxResults($maxResults);
        }

        $result = $query->getArrayResult();

        $ids = array_map(function($r){
            return $r['id'];
        },$result);

        if(count($ids) > 0){
            $qb = $this->em->createQueryBuilder();
            $qb
                ->select('m')
                ->from($entityNamespace,'m')
                ->where($qb->expr()->in('m.id',$ids))
                ->orderBy('m.'.$this->config['media_entity']['order_by_field'],$this->config['media_entity']['order_by_direction']);

            return $qb->getQuery()->getArrayResult();
        }

        return array();
    }

    public function IsRegisterPeriod($competitionId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('partial c.{id,register_init_date,register_end_date}')
            ->from('ATMCompetitionBundle:Competition','c')
            ->where($qb->expr()->eq('c.id',$competitionId));

        $result = $qb->getQuery()->getArrayResult();

        if(isset($result[0])){
            $initTs = $result[0]['register_init_date']->getTimestamp();
            $endTs = $result[0]['register_end_date']->getTimestamp();

            $currentDate = new DateTime();
            $currentTs = $currentDate->getTimestamp();

            if($currentTs >= $initTs && $currentTs <= $endTs){
                return true;
            }
            return false;
        }
        return null;
    }

    public function isCompetitionActive($competitionId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('partial c.{id,init_date,end_date}')
            ->from('ATMCompetitionBundle:Competition','c')
            ->where($qb->expr()->eq('c.id',$competitionId));

        $result = $qb->getQuery()->getArrayResult();

        if(isset($result[0])){
            $initTs = $result[0]['init_date']->getTimestamp();
            $endTs = $result[0]['end_date']->getTimestamp();

            $currentDate = new DateTime();
            $currentTs = $currentDate->getTimestamp();

            if($currentTs >= $initTs && $currentTs <= $endTs){
                return true;
            }
            return false;
        }
        return null;
    }

    public function contestantTotalVotes($contestantId){
        $count = $this->searchVotes->search(array(
            'contestant_id' => $contestantId,
            'count' => true
        ));

        return $count['count'];
    }

    public function getCurrentCompetition(){
        return $this->searchCompetition->getCurrentCompetition();
    }

    public function getCompetitionContestants($params){
        $contestants = $this->searchContestants->search($params);

        return $contestants;
    }

    public function userAlreadyVoted($competitionId,$user){
        $vote = $this->searchVotes->search(array(
            'email' => $user->getEmail(),
            'competition_id' => $competitionId,
            'count' => true
        ));

        return $vote['count'] > 0 ? true : false;
    }

    public function getBigImagePath($imagePath){
        $imageName = basename($imagePath);
        return str_replace($imageName,'u_'.$imageName,$imagePath);
    }

    public function getCompetitionIsFinished($competitionId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('partial c.{id,init_date,end_date}')
            ->from('ATMCompetitionBundle:Competition','c')
            ->where($qb->expr()->eq('c.id',$competitionId));

        $result = $qb->getQuery()->getArrayResult();

        if(isset($result[0])){
            $endTs = $result[0]['end_date']->getTimestamp();

            $currentDate = new DateTime();
            $currentTs = $currentDate->getTimestamp();

            if($currentTs > $endTs){
                return true;
            }
            return false;
        }
        return null;
    }
}