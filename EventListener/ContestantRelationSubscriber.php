<?php

namespace ATM\CompetitionBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class ContestantRelationSubscriber implements EventSubscriber{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata
        );
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if ($metadata->getName() != 'ATM\CompetitionBundle\Entity\Contestant') {
            return;
        }

        $metadata->mapManyToOne(array(
            'targetEntity' => $this->config['user'],
            'fieldName' => 'user',
            'joinColumns' => array(
                array(
                    'name' => 'user_id',
                    'referencedColumnName' => 'id'
                )
            )
        ));

        if(!is_null($this->config['media_entity']['namespace'])){
            $metadata->mapOneToOne(array(
                'targetEntity' => $this->config['media_entity']['namespace'],
                'fieldName' => 'media',
                'joinColumns' => array(
                    array(
                        'name' => 'media_id',
                        'referencedColumnName' => 'id'
                    )
                )
            ));
        }
    }
}