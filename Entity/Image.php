<?php

namespace ATM\CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_competition_image")
 */
class Image
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="path", type="string", length=255, nullable=false)
     */
    private $path;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    /**
     * @ORM\Column(name="is_main", type="boolean", nullable=false)
     */
    private $isMain;

    /**
     * @ORM\ManyToOne(targetEntity="Contestant", inversedBy="images")
     */
    protected $contestant;

    public function __construct(){
        $this->creation_date = new DateTime();
        $this->isMain = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getContestant()
    {
        return $this->contestant;
    }

    public function setContestant($contestant)
    {
        $this->contestant = $contestant;
    }

    public function getisMain()
    {
        return $this->isMain;
    }

    public function setIsMain($isMain)
    {
        $this->isMain = $isMain;
    }
}