<?php

namespace ATM\CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use \DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_competition_contestant")
 */
class Contestant{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="Competition", inversedBy="contestants")
     */
    protected $competition;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="contestant", cascade={"remove"})
     */
    protected $images;

    protected $media;

    protected $user;

    public function __construct()
    {
        $this->creation_date = new DateTime();
        $this->images = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getCompetition()
    {
        return $this->competition;
    }

    public function setCompetition($competition)
    {
        $this->competition = $competition;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia($media)
    {
        $this->media = $media;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function addImage($image){
        $this->images->add($image);
    }

    public function removeImage($image){
        $this->images->removeElement($image);
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
}