<?php

namespace ATM\CompetitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ATM\CompetitionBundle\Helpers\Canonicalizer;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_competition")
 */
class Competition
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="canonical", type="string", length=255, nullable=false)
     */
    private $canonical;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    /**
     * @ORM\Column(name="init_date", type="datetime", nullable=false)
     */
    private $init_date;

    /**
     * @ORM\Column(name="end_date", type="datetime", nullable=false)
     */
    private $end_date;

    /**
     * @ORM\Column(name="register_init_date", type="datetime", nullable=false)
     */
    private $register_init_date;

    /**
     * @ORM\Column(name="register_end_date", type="datetime", nullable=false)
     */
    private $register_end_date;

    /**
     * @ORM\Column(name="rules", type="text", nullable=false)
     */
    private $rules;

    /**
     * @ORM\Column(name="prices", type="text", nullable=false)
     */
    private $prices;

    /**
     * @ORM\Column(name="folder",  type="string", length=255, nullable=false)
     */
    private $folder;

    /**
     * @ORM\Column(name="image", type="text", nullable=false)
     */
    private $image;

    /**
     * @ORM\Column(name="image_mobile", type="text", nullable=true)
     */
    private $image_mobile;

    /**
     * @ORM\Column(name="image_middle", type="text", nullable=true)
     */
    private $image_middle;

    /**
     * @ORM\OneToMany(targetEntity="Contestant", mappedBy="competition", cascade={"remove"})
     */
    protected $contestants;

    public function __construct()
    {
        $this->creation_date = new DateTime();
        $this->contestants = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        $this->canonical = Canonicalizer::canonicalize($name);
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getInitDate()
    {
        return $this->init_date;
    }

    public function setInitDate($init_date)
    {
        $this->init_date = $init_date;
    }

    public function getEndDate()
    {
        return $this->end_date;
    }

    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function setRules($rules)
    {
        $this->rules = $rules;
    }

    public function getPrices()
    {
        return $this->prices;
    }

    public function setPrices($prices)
    {
        $this->prices = $prices;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImageMobile()
    {
        return $this->image_mobile;
    }

    public function setImageMobile($image_mobile)
    {
        $this->image_mobile = $image_mobile;
    }

    public function getContestants(){
        return $this->contestants;
    }

    public function addContestant($contestant){
        $this->contestants->add($contestant);
    }

    public function removeContestant($contestant){
        $this->contestants->removeElement($contestant);
    }

    public function getFolder()
    {
        return $this->folder;
    }

    public function setFolder($folder)
    {
        $this->folder = $folder;
    }

    public function getCanonical()
    {
        return $this->canonical;
    }

    public function setCanonical($canonical)
    {
        $this->canonical = $canonical;
    }

    public function getRegisterInitDate()
    {
        return $this->register_init_date;
    }

    public function setRegisterInitDate($register_init_date)
    {
        $this->register_init_date = $register_init_date;
    }

    public function getRegisterEndDate()
    {
        return $this->register_end_date;
    }

    public function setRegisterEndDate($register_end_date)
    {
        $this->register_end_date = $register_end_date;
    }

    public function getImageMiddle()
    {
        return $this->image_middle;
    }

    public function setImageMiddle($image_middle)
    {
        $this->image_middle = $image_middle;
    }
}