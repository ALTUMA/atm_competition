<?php

namespace ATM\CompetitionBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

class SearchCompetition{
    private $em;
    private $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function search($options){
        $defaultOptions = array(
            'name' => null,
            'date' => null,
            'ids' => null,
            'pagination' => null,
            'page' => 1,
            'is_finished' => null,
            'is_register_period' => null,
            'order_by_field' => 'name',
            'order_by_direction' => 'DESC',
            'max_results' => null
        );

        $options = array_merge($defaultOptions, $options);

        $qbIds = $this->em->createQueryBuilder();
        $qbIds
            ->select('partial c.{id}')
            ->from('ATMCompetitionBundle:Competition','c');

        if(!is_null($options['name'])){
            $qbIds->andWhere($qbIds->expr()->like('c.name',$qbIds->expr()->literal('%'.$options['name'].'%')));
        }

        if(!is_null($options['date']) && !is_null($options['is_finished'])){

            $orX = $qbIds->expr()->orX();

            $orX->add($qbIds->expr()->lt('c.end_date',$qbIds->expr()->literal($options['date'])));

            $andX = $qbIds->expr()->andX();
            $andX->add($qbIds->expr()->lte('c.init_date',$qbIds->expr()->literal($options['date'])));
            $andX->add($qbIds->expr()->gte('c.end_date',$qbIds->expr()->literal($options['date'])));

            if(!is_null($options['is_register_period'])){
                $andXRegister = $qbIds->expr()->andX();
                $andXRegister->add($qbIds->expr()->lte('c.register_init_date',$qbIds->expr()->literal($options['date'])));
                $andXRegister->add($qbIds->expr()->gte('c.register_end_date',$qbIds->expr()->literal($options['date'])));
                $orX->add($andXRegister);
            }

            $orX->add($andX);
            $qbIds->andWhere($orX);
        }else{
            if(!is_null($options['is_finished'])){
                $qbIds->orWhere($qbIds->expr()->lt('c.end_date',$qbIds->expr()->literal($options['date'])));
            }

            if(!is_null($options['date'])){
                $qbIds->andWhere($qbIds->expr()->lte('c.init_date',$qbIds->expr()->literal($options['date'])));
                $qbIds->andWhere($qbIds->expr()->gte('c.end_date',$qbIds->expr()->literal($options['date'])));
            }

            if(!is_null($options['is_register_period'])){
                $qbIds->orWhere(
                    $qbIds->expr()->andX(
                        $qbIds->expr()->lte('c.register_init_date',$qbIds->expr()->literal($options['date'])),
                        $qbIds->expr()->gte('c.register_end_date',$qbIds->expr()->literal($options['date']))
                    )
                );
            }
        }



        if(!is_null($options['ids'])){
            $qbIds->andWhere($qbIds->expr()->in('c.id',$options['ids']));
        }

        $qbIds->orderBy('c.'.$options['order_by_field'],$options['order_by_direction']);
//dump($qbIds->getQuery()->getDQL());die;
        $pagination = null;
        if(!is_null($options['pagination'])){
            $arrIds = array_map(function($p){
                return $p['id'];
            },$qbIds->getQuery()->getArrayResult());

            $pagination = $this->paginator->paginate(
                $arrIds,
                is_null($options['page']) ? 1 : $options['page'],
                is_null($options['max_results']) ? 10 : $options['max_results']
            );

            $ids = $pagination->getItems();
        }else {
            $query = $qbIds->getQuery();
            if(!is_null($options['max_results'])){
                $query->setMaxResults($options['max_results']);
            }

            $ids = array_map(function ($p) {
                return $p['id'];
            }, $query->getArrayResult());
        }

        $competitions = array();
        if(count($ids) > 0){
            $qb = $this->em->createQueryBuilder();
            $qb
                ->select('partial c.{id,name,creation_date,init_date,end_date,rules,prices,image,image_mobile,image_middle,canonical,register_init_date,register_end_date}')
                ->from('ATMCompetitionBundle:Competition','c')
                ->where($qb->expr()->in('c.id',$ids))
                ->orderBy('c.'.$options['order_by_field'],$options['order_by_direction']);

            $competitions = $qb->getQuery()->getArrayResult();
        }

        return array(
            'results' => $competitions,
            'pagination' => $pagination
        );

    }

    public function getCurrentCompetition()
    {
        $qb = $this->em->createQueryBuilder();
        $current_competition = $qb
            ->select('c')
            ->from('ATMCompetitionBundle:Competition','c')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->lte('DATE(c.register_init_date)', 'CURRENT_DATE()'),
                    $qb->expr()->gte('DATE(c.end_date)', 'CURRENT_DATE()')
                )
            )
            ->getQuery()->getArrayResult();
        return $current_competition ? $current_competition[0] : false;
    }
}