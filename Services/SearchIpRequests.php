<?php

namespace ATM\CompetitionBundle\Services;

use Doctrine\ODM\MongoDB\DocumentManager;
use ATM\CompetitionBundle\Document\CompetitionIpRequest;
use \MongoDate;

class SearchIpRequests{
    private $dm;

    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }


    public function search($ip,$date){
        $qb = $this->dm->createQueryBuilder(CompetitionIpRequest::class);

        $qb
            ->select('id','ip','creation_date')
            ->field('ip')->equals($ip)
            ->field('creation_date')->equals(new MongoDate($date->format('U'),$date->format('u')));

        return $qb->getQuery()->execute()->count();
    }
}