<?php

namespace ATM\CompetitionBundle\Controller;

use ATM\CompetitionBundle\Services\SearchContestants;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ATM\CompetitionBundle\Entity\Competition;
use ATM\CompetitionBundle\Form\CompetitionType;
use ATM\CompetitionBundle\Services\SearchCompetition;
use ATM\CompetitionBundle\Services\SearchVotes;
use ATM\CompetitionBundle\Services\ImageManager;

class CompetitionController extends Controller
{
    /**
     * @Route("/competition/create", name="atm_competition_create")
     */
    public function createCompetitionAction(){
        $config = $this->getParameter('atm_competition_config');
        $competition = new Competition();

        $form = $this->createForm(CompetitionType::class,$competition);
        $request = $this->get('request_stack')->getCurrentRequest();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $config = $this->getParameter('atm_competition_config');
            if(!is_dir($this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'])){
                mkdir($this->get('kernel')->getRootDir().'/../web/'.$config['media_folder']);
            }

            $competitionFolder = md5(uniqid());
            $pathToFolder = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/'.$competitionFolder;
            mkdir($pathToFolder);

            $file = $request->files->get('flImage');
            $filename = md5(uniqid()).'.'.$file->guessExtension();
            $file->move($pathToFolder,$filename);
            $competition->setFolder($competitionFolder);
            $competition->setImage($config['media_folder'].'/'.$competitionFolder.'/'.$filename);

            $mobileFile = $request->files->get('flImageMobile');
            $mobileImageFile = md5(uniqid()).'.'.$mobileFile->guessExtension();
            $mobileFile->move($pathToFolder,$mobileImageFile);
            $competition->setImageMobile($config['media_folder'].'/'.$competitionFolder.'/'.$mobileImageFile);

            $middleFile = $request->files->get('flImageMiddle');
            $middleImageFile = md5(uniqid()).'.'.$middleFile->guessExtension();
            $middleFile->move($pathToFolder,$middleImageFile);
            $competition->setImageMiddle($config['media_folder'].'/'.$competitionFolder.'/'.$middleImageFile);

            $registerEndDate = $competition->getRegisterEndDate()->modify('23 hours')->modify('59 minutes')->modify('59 seconds');
            $competition->setRegisterEndDate($registerEndDate);

            $endDate = $competition->getEndDate()->modify('+23 hours')->modify('+59 minutes')->modify('+59 seconds');
            $competition->setEndDate($endDate);

            $defaultTemplate = $this->get('kernel')->getRootDir().'/../vendor/atm/competitionbundle/Resources/views/Frontend/default_template.html.twig';
            $competitionTemplate = $this->get('kernel')->getRootDir().'/Resources/ATMCompetitionBundle/views/Frontend/'.$competition->getCanonical().'.html.twig';
            $twigCode = file_get_contents($defaultTemplate);
            $twigCode = str_replace('%sitename%',$config['site_domain'],$twigCode);
            file_put_contents($competitionTemplate,$twigCode);

            $defaultVoteLink = $this->get('kernel')->getRootDir().'/../vendor/atm/competitionbundle/Resources/views/Mail/default_vote_link.html.twig';
            $competitionVoteLink = $this->get('kernel')->getRootDir().'/Resources/ATMCompetitionBundle/views/Mail/'.$competition->getCanonical().'_vote_link.html.twig';
            $twigCodeLink = file_get_contents($defaultVoteLink);
            $twigCodeLink = str_replace('%sitename%',$config['site_domain'],$twigCodeLink);
            file_put_contents($competitionVoteLink,$twigCodeLink);

            $em = $this->getDoctrine()->getManager();
            $em->persist($competition);
            $em->flush();

            return new RedirectResponse($this->get('router')->generate('atm_competition_list'));
        }

        return $this->render('ATMCompetitionBundle:Competition:create.html.twig',array(
            'form' => $form->createView(),
            'froala_key' => $config['froala_key']
        ));
    }

    /**
     * @Route("/competition/edit/{competitionId}", name="atm_competition_edit")
     */
    public function editCompetitionAction($competitionId){
        $config = $this->getParameter('atm_competition_config');
        $em = $this->getDoctrine()->getManager();
        $competition = $em->getRepository('ATMCompetitionBundle:Competition')->findOneById($competitionId);

        $form = $this->createForm(CompetitionType::class,$competition);
        $request = $this->get('request_stack')->getCurrentRequest();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pathToFolder = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/'.$competition->getFolder();

            $file = $request->files->get('flImage');
            if(!is_null($file)){

                if(file_exists($this->get('kernel')->getRootDir().'/../web/'.$competition->getImage())){
                    unlink($this->get('kernel')->getRootDir().'/../web/'.$competition->getImage());
                }

                $file = $request->files->get('flImage');
                $extension = $file->guessExtension();
                $filename = md5(uniqid()).'.'.$extension;
                $file->move($pathToFolder,$filename);

                $competition->setImage($config['media_folder'].'/'.$competition->getFolder().'/'.$filename);
            }

            $mobileFile = $request->files->get('flImageMobile');
            if($mobileFile){
                if(!is_null($competition->getImageMobile()) && file_exists($this->get('kernel')->getRootDir().'/../web/'.$competition->getImageMobile())){
                    unlink($this->get('kernel')->getRootDir().'/../web/'.$competition->getImageMobile());
                }
                $mobileImageFile = md5(uniqid()).'.'.$mobileFile->guessExtension();
                $mobileFile->move($pathToFolder,$mobileImageFile);
                $competition->setImageMobile($config['media_folder'].'/'.$competition->getFolder().'/'.$mobileImageFile);
            }

            $middleFile = $request->files->get('flImageMiddle');
            if($middleFile){
                if(!is_null($competition->getImageMiddle()) && file_exists($this->get('kernel')->getRootDir().'/../web/'.$competition->getImageMiddle())){
                    unlink($this->get('kernel')->getRootDir().'/../web/'.$competition->getImageMiddle());
                }
                $middleImageFile = md5(uniqid()).'.'.$middleFile->guessExtension();
                $middleFile->move($pathToFolder,$middleImageFile);
                $competition->setImageMiddle($config['media_folder'].'/'.$competition->getFolder().'/'.$middleImageFile);
            }

            $registerEndDate = $competition->getRegisterEndDate()->modify('23 hours')->modify('59 minutes')->modify('59 seconds');
            $competition->setRegisterEndDate($registerEndDate);

            $endDate = $competition->getEndDate()->modify('+23 hours')->modify('+59 minutes')->modify('+59 seconds');
            $competition->setEndDate($endDate);

            $em->persist($competition);
            $em->flush();

            return new RedirectResponse($this->get('router')->generate('atm_competition_list'));
        }

        return $this->render('ATMCompetitionBundle:Competition:edit.html.twig',array(
            'form' => $form->createView(),
            'competition' => $competition,
            'froala_key' => $config['froala_key']
        ));
    }

    /**
     * @Route("/competition/delete/{competitionId}", name="atm_competition_delete")
     */
    public function deleteCompetitionAction($competitionId){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_competition_config');

        $competition = $em->getRepository('ATMCompetitionBundle:Competition')->findOneById($competitionId);

        $fileSystem = new Filesystem();
        $fileSystem->remove(array('folder',$this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/'.$competition->getFolder()));

        $em->remove($competition);
        $em->flush();

        $dm = $this->get('doctrine_mongodb')->getManager();
        $votes = $dm->getRepository('ATMCompetitionBundle:CompetitionVote')->findBy(array('competition_id'=> $competitionId));
        $count = 0;
        foreach ($votes as $vote){
            $dm->remove($vote);

            if($count % 500 == 0){
                $dm->flush();
            }
            $count++;
        }
        $dm->flush();

        return new RedirectResponse($this->get('router')->generate('atm_competition_list'));
    }

    /**
     * @Route("/competition/list/{page}", name="atm_competition_list", defaults={"page":1})
     */
    public function competitionListAction($page){

        $params = array(
            'page' => $page,
            'pagination' => true,
            'order_by_field' => 'init_date'
        );

        $competitions = $this->get(SearchCompetition::class)->search($params);

        return $this->render('ATMCompetitionBundle:Competition:list.html.twig',array(
            'competitions' => $competitions['results'],
            'pagination' => $competitions['pagination'],
        ));
    }

    /**
     * @Route("/manage/votes/{competitionId}/{page}", name="atm_competition_manage_votes", defaults={"page":1,"competitionId":null}, options={"expose"=true})
     */
    public function manageVotesAction($competitionId,$page){
        $em = $this->getDoctrine()->getManager();

        $competitions = $em->getRepository('ATMCompetitionBundle:Competition')->findAll();


        return $this->render('ATMCompetitionBundle:Competition:manageVotes.html.twig',array(
            'competitions' => $competitions,
            'competition_id' => $competitionId,
            'page' => $page
        ));
    }

    public function searchVotesAction($params){

        $votes = $this->get(SearchVotes::class)->search($params);

        $contestantIds = array_map(function($v){
            return $v->getContestantId();
        },$votes['results']);

        $names = array();
        if(count($contestantIds) > 0){
            $contestants = $this->get(SearchContestants::class)->search(array('ids'=>$contestantIds));

            $names = array();
            foreach($contestants['results'] as $contestant){
                $names[$contestant['id']] = $contestant['name'];
            }
        }

        return $this->render('ATMCompetitionBundle:Competition:votes_results.html.twig',array(
            'votes' => $votes['results'],
            'pagination' => $votes['pagination'],
            'contestant_name' => $names
        ));
    }

    /**
     * @Route("/remove/contestant/{contestantId}", name="atm_competition_remove_contestant")
     */
    public function removeContestantAction($contestantId){
        $em = $this->getDoctrine()->getManager();

        $contestant = $em->getRepository('ATMCompetitionBundle:Contestant')->findOneById($contestantId);

        $competition = $contestant->getCompetition();

        $em->remove($contestant);
        $em->flush();

        return new RedirectResponse($this->get('router')->generate('atm_competition_landing',array(
            'canonical' => $competition->getCanonical(),
            'competitionId' => $competition->getId()
        )));
    }

    /**
     * @Route("/list/anonymous/users/{competitionId}/{page}", name="atm_competition_anonymous_users_list", defaults={"page":1,"competitionId":null}, options={"expose"=true})
     */
    public function anonymousUsersListAction($competitionId, $page){
        $em = $this->getDoctrine()->getManager();

        $total = $pagination = $anonymous_users = null;

        if(!is_null($competitionId)){
            $qb = $em->createQueryBuilder();
            $qb
                ->select('a')
                ->from('ATMCompetitionBundle:AnonymousUser','a')
                ->join('a.competition','c','WITH',$qb->expr()->eq('c.id',$competitionId))
                ->orderBy('a.creation_date','DESC')
            ;

            $pagination = $this->get('knp_paginator')->paginate(
                $qb->getQuery()->getArrayResult(),
                $page,
                20
            );

            $anonymous_users = $pagination->getItems();

            $qbTotal = $em->createQueryBuilder();
            $qbTotal
                ->select('count(a.id) as total')
                ->from('ATMCompetitionBundle:AnonymousUser','a')
                ->join('a.competition','c','WITH',$qb->expr()->eq('c.id',$competitionId));

            $total = $qbTotal->getQuery()->getArrayResult();
        }

        $competitions = $em->getRepository('ATMCompetitionBundle:Competition')->findAll();

        return $this->render('ATMCompetitionBundle:Competition:anonymous_users.html.twig',array(
            'anonymous_users' => $anonymous_users,
            'pagination' => $pagination,
            'page' => $page,
            'competitionId' => $competitionId,
            'competitions' => $competitions,
            'total' => !is_null($total) ? $total[0]['total'] : null
        ));
    }

    /**
     * @Route("/export/anonymous/users/{competitionId}", name="atm_competition_export_anonymous_users")
     */
    public function exportAnonymousUsersAction($competitionId){
        $em = $this->getDoctrine()->getManager();

        $competition = $em->getRepository('ATMCompetitionBundle:Competition')->findOneById($competitionId);

        $qb = $em->createQueryBuilder();

        $qb
            ->select('a')
            ->from('ATMCompetitionBundle:AnonymousUser','a')
            ->join('a.competition','c','WITH',$qb->expr()->eq('c.id',$competitionId))
            ->orderBy('a.creation_date','DESC');
        $anonymousUsers = $qb->getQuery()->getArrayResult();

        $fp = fopen('php://temp', 'w');
        foreach ($anonymousUsers as $anonymousUser) {
            $username = explode('@',$anonymousUser['email']);
            $columns = array($username[0],$anonymousUser['email']);
            fputcsv($fp, $columns);
        }

        rewind($fp);
        $response = new Response(stream_get_contents($fp));
        fclose($fp);

        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="'.str_replace(array('!',' '),'',$competition->getName()).'_anonymous_users.csv"');

        return $response;

    }
}

