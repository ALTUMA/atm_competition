<?php

namespace ATM\CompetitionBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use \DateTime;

class CompetitionEmailsCommand extends ContainerAwareCommand{

    protected function configure()
    {
        $this->setName('competition:send_mail:start_end');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');

        $qbCompetitions = $em->createQueryBuilder();

        $qbCompetitions
            ->select('c')
            ->from('ATMCompetitionBundle:Competition','c');

        $competitions = $qbCompetitions->getQuery()->getResult();
        $currentDate =  new DateTime();
        foreach($competitions as $competition){

            if($competition->getInitDate()->format('dmY') == $currentDate->format('dmY')){
                $this->sendEmail('start_date',$competition);
            }
            if($competition->getEndDate()->format('dmY') == $currentDate->format('dmY')){
                $this->sendEmail('end_date',$competition);
            }
        }
    }

    private function sendEmail($type,$competition){
        $container = $this->getContainer();
        $templating = $container->get('templating');
        $em = $container->get('doctrine.orm.default_entity_manager');
        $config = $container->getParameter('atm_competition_config');

        $qbUsers = $em->createQueryBuilder();
        $qbUsers
            ->select('c')
            ->addSelect('u')
            ->from('ATMCompetitionBundle:Contestant','c')
            ->join('c.user','u')
            ->where(
                $qbUsers->expr()->andX(
                    $qbUsers->expr()->eq('u.enabled',1),
                    $qbUsers->expr()->eq('u.locked',0)
                )
            );


        $contestants = $qbUsers->getQuery()->getArrayResult();
        foreach($contestants as $contestant){
            $user = $contestant['user'];

            switch($type){
                case 'start_date':
                    $subject = $config['email_from_name'].' - The '.$competition->getName().' competition has started';
                    $body = $templating->render('ATMCompetitionBundle:Mail:competition_start.html.twig',array(
                        'competition' => $competition,
                        'user' => $user
                    ));
                    break;
                case 'end_date':
                    $subject = $config['email_from_name'].' - The '.$competition->getName().' competition has finished';
                    $body = $templating->render('ATMCompetitionBundle:Mail:competition_end.html.twig',array(
                        'competition' => $competition,
                        'user' => $user
                    ));
                    break;
            }

            $container->get('atm_mailer')->send(
                $user['email'],
                $subject,
                $body
            );
        }
    }
}