<?php

namespace ATM\CompetitionBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder
            ->root('atm_competition')
                ->children()
                    ->scalarNode('user')->isRequired()->end()
                    ->scalarNode('media_folder')->isRequired()->end()
                    ->scalarNode('contestant_role')->isRequired()->end()
                    ->scalarNode('froala_key')->isRequired()->end()
                    ->scalarNode('watermark_image_small')->isRequired()->end()
                    ->scalarNode('watermark_image_medium')->isRequired()->end()
                    ->scalarNode('watermark_image_big')->isRequired()->end()
                    ->integerNode('main_picture_width')->isRequired()->end()
                    ->integerNode('main_picture_height')->isRequired()->end()
                    ->scalarNode('email_from_name')->isRequired()->end()
                    ->scalarNode('email_from_address')->isRequired()->end()
                    ->scalarNode('email_subject')->isRequired()->end()
                    ->scalarNode('site_domain')->isRequired()->end()
                    ->arrayNode('media_entity')->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('namespace')->isRequired()->defaultValue(null)->end()
                            ->scalarNode('user_field_name')->isRequired()->defaultValue('user')->end()
                            ->scalarNode('order_by_field')->isRequired()->end()
                            ->scalarNode('order_by_direction')->isRequired()->end()
                        ->end()
                    ->end()
                    ->arrayNode('messages')->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('not_allowed')->defaultValue('You are not allowed to participate in this competition.')->end()
                            ->scalarNode('vote_link_sent')->defaultValue('We sent you an email with a link for voting.')->end()
                            ->scalarNode('already_voted')->defaultValue('You already voted in this competition.')->end()
                            ->scalarNode('email_not_validated')->defaultValue('Your email was not validated, please try again.')->end()
                            ->scalarNode('voted_successfully')->defaultValue('Thanks for voting.')->end()
                        ->end()
                    ->end()
                ->end();

        return $treeBuilder;
    }
}
