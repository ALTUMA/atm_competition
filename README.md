# Installation #

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require atm/competitionbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new ATM\CompetitionBundle\ATMCompetitionBundle(),
    ];
}
```

# Configuration sample #
``` yml
# app/config/config.yml
  
atm_competition:
    user: User namespace
    media_folder: path to media folder
    contestant_role: role for the contestants
    froala_key: froala key
    main_picture_width: width for images
    main_picture_height: height for images
    watermark_image_small: 'watermark_small.png'
    watermark_image_medium: 'watermark_medium.png'
    watermark_image_big: 'watermark_big.png'
    email_from_name: Name for the from email address
    email_from_address: from email address
    email_subject: Subject for the email when a anonymous user vote
    site_domain: site domain
    messages:
        not_allowed: message when a contestant is not allowed to join a competition 
        vote_link_sent: message when a link sent to an anonymous user for voting
        already_voted: message shown when a user already voted
        email_not_validated: message when the email of an anonymous user is not validated
        voted_successfully: message shown when a user votes
    media_entity:
        namespace: media entity namespace
        order_by_field: field for sorting results
        order_by_direction: sorting direction
```

# Routing #
Append to the main routing file:
```yml
# app/config/routing.yml

atm_competition:
    resource: "@ATMCompetitionBundle/Controller/CompetitionController.php"
    type:     annotation
    prefix:   /

# The frontend routing should be not protected by the firewall
atm_competition_frontend:
    resource: "@ATMCompetitionBundle/Controller/FrontendController.php"
    type:     annotation
    prefix:   /
```
# Usage #
After creating a new competition you can customize the landing page of it by creating a twig
file using competition's canonical in the following path.(___Imagine that the competition is
called spring competition it's canonical will be springcompetition___): 
ATMCompetitionBundle\views\Frontend\competitioncanonical.html.twig

These are the variables that are used in that view:

    - competition: competition data as an array
    - contestant_role: role from the configuration that the contestant have to had in order to join the competition
    - page: needed in case to show a list of contestants.
    
For showing the contestants of a competition you can use the following function:
```twig
    {{ render(controller('ATMCompetitionBundle:Frontend:searchContestants',{
        'params' : {
            'page': page,
            'competition_id': competition.id,
            'order_by_field': 'name',
            'order_by_direction': 'DESC',
            'max_results' : 10
        }
    })) }} 
```

# Services
There are 3 services for searching.

#### SearchCompetition ####
This service accept the following parameters for searching competitions:
```php
$defaultOptions = array(
    'name' => null,
    'date' => null,
    'ids' => null,
    'pagination' => null,
    'page' => 1,
    'order_by_field' => 'name',
    'order_by_direction' => 'DESC',
    'max_results' => null
);
```
It will return an array with 2 keys:

    - results: results from the query
    - pagination: pagination object in case that pagination is true
 
#### SearchContestants ####
This service accept the following parameters for searching contestants:
```php
$defaultOptions = array(
    'name' => null,
    'date' => null,
    'ids' => null,
    'competition' => null,
    'pagination' => null,
    'page' => 1,
    'order_by_field' => 'name',
    'order_by_direction' => 'DESC',
    'max_results' => null
);
```
It will return an array with 2 keys:

    - results: results from the query
    - pagination: pagination object in case that pagination is true

___If you put order_by_field = 'votes' you will get the contestants sorted by it's total votes.___

#### SearchVotes ####
This service accept the following parameters for searching votes:
```php
$defaultOptions = array(
    'email' => null,
    'competition_id' => null,
    'contestant_id' => null,
    'creation_date' => null,
    'ids' => null,
    'sorting' => 'DESC',
    'sorting_field' => null,
    'date_limit' => array(
        'min' => null,
        'max' => null
    ),
    'limit' => null,
    'hydrate' => true,
    'page' => 1,
    'max_results' => null,
    'pagination' => null,
    'count' => null
);
```
It will return an array with 2 keys:

    - results: results from the query
    - pagination: pagination object in case that pagination is true

If count is true it will return an array with only 1 key:

    - count: total amount of votes of a contestant
   
# Twig Extensions #
These are the following functions that you can use in twig:
```php
    - ATMCompIsContestant($competitionId,$userId): checks if a user is a contestant of a competition.
    - ATMCompGetLastUserMedia($maxResults,$userId): gets the media based on the media namespace in the configuration.
    - ATMCompIsRegisterPeriod($competitionId): checks if it's the register period of a competition.
    - ATMCompIsCompetitionActive($competitionId): checks if a competition is active.
    - ATMCompContestantTotalVotes($contestantId): get contestant total votes.
```

# Mail sent to anonymous user #
If the user that votes is not logged in the system sends an email to it with a link for voting. You can customize 
the mail template creating a twig file in the views/Mail called competitioncanonical_vote_link.html.twig. The system passes
a variable called {{ vote_link }}.

In the  views/Mail folder there is an example template called thegoodone_vote_link.html.twig.

# Example #
In the views/Frontend you can see an example of a competition landing page called thegoodone.html.twig.